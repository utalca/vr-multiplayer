﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HostGame : MonoBehaviour {

    [SerializeField]
    private uint roomSize = 2;

    private string roomName;

    private NetworkManager networkManager;

    void Start()
    {
        networkManager = NetworkManager.singleton;

        if (networkManager.matchMaker == null)
        {
            networkManager.StartMatchMaker();
        }
    }

    public void SetRoomSize(float value)
    {
        roomSize = (uint)value;
    }

    public void SetRoomName(string roomName)
    {
        this.roomName = roomName;
    }

    public void CreateRoom()
    {
        if (roomName != null && roomName != "")
        {
            Debug.Log("Creating Room: " + roomName + " for " + roomSize + " players.");

            networkManager.matchMaker.CreateMatch(roomName, roomSize, true, "", "", "", 0, 0, networkManager.OnMatchCreate);

       //     GameManager.Singleton.currentRoomName = roomName;
       //     GameManager.Singleton.currentRoomSize = (int)roomSize;
        }
    }
	
}
