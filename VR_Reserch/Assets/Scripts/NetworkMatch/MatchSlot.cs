﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class MatchSlot : MonoBehaviour {

    public delegate void JoinRoomDelegate(MatchInfoSnapshot _match);
    public JoinRoomDelegate joinRoomCallback;

    [SerializeField]
    private Text roomName;

    MatchInfoSnapshot _info;

    public void Setup(MatchInfoSnapshot info, JoinRoomDelegate _joinRoomDelegate)
    {
        _info = info;

        joinRoomCallback = _joinRoomDelegate;

        if (info.currentSize == info.maxSize)
        {
            roomName.text = info.name + "(Playing)";
        }
        {
            roomName.text = info.name + "(" + info.currentSize + "/" + info.maxSize + ")";
        }
    }

    public  void JoinGame()
    {
        joinRoomCallback.Invoke(_info);
    }
}
