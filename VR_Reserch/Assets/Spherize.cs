using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spherize : MonoBehaviour {

	public MeshFilter mf;
	private Mesh m;

	public float rad = 1;

	[ContextMenu("Spherize Cube")]
	public void SpherizeCube()
	{
		m = Instantiate(mf.sharedMesh);
		mf.sharedMesh = m;
		Vector3[] vertices = mf.sharedMesh.vertices;
		
			for(int i = 0; i < vertices.Length; i++){
				
			vertices[i] = vertices[i] * (Random.Range(0f,0.1f));
		}
		mf.sharedMesh.vertices = vertices;
		mf.sharedMesh.RecalculateNormals();
	}
}
